<?php declare(strict_types=1);

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
        'customer_id' => $faker->randomNumber(),
        'amount' => $faker->numberBetween(0,1000),
        'date'=>$faker->dateTime()
    ];
});
