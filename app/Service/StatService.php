<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 10.04.18
 * Time: 23:27
 */

namespace App\Service;


use App\Stat;
use Carbon\Carbon;

class StatService
{

    public function execute(Carbon $from, Carbon $to)
    {
        $sum = \DB::table('transactions')
            ->where('date', '>=', $from)
            ->where('date', '<', $to)
            ->sum('amount');

        $stat = Stat::create(['sum' => $sum, 'date' => $from]);

        return $stat;

    }

}