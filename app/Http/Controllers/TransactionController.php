<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
//        print_r($request->all());
        $query = Transaction::query();
        if ($customerId = $request->get('customer_id')) {
            $query->where('customer_id', $customerId);
        }
        if ($amount = $request->get('amount')) {
            $query->where('amount', $amount);
        }
        if ($date = $request->get('date')) {
            $query->where('date', $date);
        }
        if ($limit = $request->get('limit')) {
            $query->limit($limit);
        }
        if ($offset = $request->get('offset')) {
            $query->offset($offset);
        }
        return $query->get();
//        if()
//            ->where('customer_id', $request->get('customer_id'))
//            ->where('amount', $request->get('amount'))
//            ->where('date', $request->get('date'))->get();
    }

    public function list()
    {
        $transactions = Transaction::all();
        return \View::make('transactions.list')
            ->with('transactions', $transactions);
    }

    public function show(Transaction $transaction)
    {
        return $transaction;
    }

    public function store(Request $request)
    {
        $transaction = Transaction::create($request->all());

        return response()->json($transaction, 201);
    }

    public function update(Request $request, Transaction $transaction)
    {
        $transaction->update($request->all());

        return response()->json($transaction, 200);
    }

    public function delete(Transaction $transaction)
    {
        $transaction->delete();

        return response()->json(null, 204);
    }
}
