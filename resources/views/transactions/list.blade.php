@extends('layouts.app')

@section('content')

    <h1>All the Transactions</h1>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>ID</td>
            <td>Customer</td>
            <td>Amount</td>
            <td>Date</td>
        </tr>
        </thead>
        <tbody>
        @foreach($transactions as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->customer_id }}</td>
                <td>{{ $value->amount }}</td>
                <td>{{ $value->date }}</td>

            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
