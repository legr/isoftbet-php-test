git clone git@bitbucket.org:legr/isoftbet-php-test.git
cd isoftbet-php-test
composer install
cp -a .env.example .env
php artisan key:generate

touch database/database.sqlite

edit .evn and add those rows:

DB_CONNECTION=sqlite
DB_DATABASE=/Users/oc/tmp/isoftbet-php-test/database/database.sqlite
APP_ENV=development


— start app
php artisan serve

Go to website
http://127.0.0.1:8000/


### Register: POST api/register 
   - Request: name, email, password, password_confirmation
   - Response: id, name, email, created_at,updated_at,api_token
 
```bash 
curl -X POST http://isoftbet.test/api/register \
 -H "Accept: application/json" \
 -H "Content-Type: application/json" \
 -d '{"name": "admin", "email": "admin@domain.com", "password": "123456", "password_confirmation": "123456"}'
```      
```json
{  
   "data":{  
      "id":1,
      "name":"admin",
      "email":"admin@domain.com",
      "created_at":"2018-04-08 10:17:41",
      "updated_at":"2018-04-10 22:07:48",
      "api_token":"jBfQaAPJovnDJ62H6piLXe7zmFzkjlyMdrFsyxztbDNMZ9t8RgMwGY7BWOFx"
   }
}
```         
                                  
### Login: POST api/login 
  - Request: email, password
  - Response: id, name, email, created_at,updated_at,api_token

```bash 
curl -X POST http://isoftbet.test/api/login \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -d "{\"email\": \"admin@domain.com\", \"password\": \"123456\" }"
```   
```bash 
{  
   "data":{  
      "id":1,
      "name":"admin",
      "email":"admin@domain.com",
      "created_at":"2018-04-08 10:17:41",
      "updated_at":"2018-04-10 22:19:55",
      "api_token":"6ox4I80xDjn32hCA1AYTLyEiq2v2uDo1zvFZECDE9Z9NyUlQM9LQObYguKGe"
   }
}
```                                      
### Logout: POST api/logout 
```bash 
curl -X POST http://isoftbet.test/api/logout \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer jBfQaAPJovnDJ62H6piLXe7zmFzkjlyMdrFsyxztbDNMZ9t8RgMwGY7BWOFx"
```   

### Adding a customer: POST api/customer 
  - Request: ​name, cnp
  - Response: ​customerId
```bash 
curl -X POST http://isoftbet.test/api/customer \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer jBfQaAPJovnDJ62H6piLXe7zmFzkjlyMdrFsyxztbDNMZ9t8RgMwGY7BWOFx" \
  -d '{"name": "customer1", "cnp": "23421423143"}'

``` 
```json
{  
   "name":"customer1",
   "cnp":"23421423143",
   "updated_at":"2018-04-10 22:16:37",
   "created_at":"2018-04-10 22:16:37",
   "id":5
}


```                                      
  
### Getting a transaction: GET api/transaction/{transactionId}
  - Response: ​transactionId, amount, date
```bash 
curl -X GET http://isoftbet.test/api/transaction/1 \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer jBfQaAPJovnDJ62H6piLXe7zmFzkjlyMdrFsyxztbDNMZ9t8RgMwGY7BWOFx"

```  
```json                                    
{  
   "id":1,
   "customer_id":1,
   "amount":"2000.30",
   "date":"2018-04-10 18:13:52",
   "created_at":"2018-04-10 18:13:52",
   "updated_at":"2018-04-10 22:24:06"
}
```                                       
  
### Getting transactions by filters: GET api/transaction
  - Request: ​customerId, amount, date, offset, limit
  - Response: an array of transactions
```bash 
curl -X GET http://isoftbet.test/api/transaction \
-H "Accept: application/json"   -H "Content-type: application/json" \
-H "Authorization: Bearer jBfQaAPJovnDJ62H6piLXe7zmFzkjlyMdrFsyxztbDNMZ9t8RgMwGY7BWOFx" \
-d '{"customer_id": "1", "amount":"100", "limit":"1", "offset":"1"}'

```    
```json
[  
   {  
      "id":3,
      "customer_id":1,
      "amount":100,
      "date":"2018-04-10 19:38:48",
      "created_at":"2018-04-10 19:38:48",
      "updated_at":"2018-04-10 19:38:48"
   }
]
```                                  
  
### Adding a transaction: POST api/transaction
  - Request: ​customerId, amount
  - Response:transactionId, customerId, amount, date
```bash 
curl -X POST http://isoftbet.test/api/transaction \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer jBfQaAPJovnDJ62H6piLXe7zmFzkjlyMdrFsyxztbDNMZ9t8RgMwGY7BWOFx" \
  -d '{"customer_id": "1", "amount":"110.30"}'

```  
```json                                    
  {  
     "customer_id":"1",
     "amount":"110.30",
     "date":{  
        "date":"2018-04-10 22:22:09.318874",
        "timezone_type":3,
        "timezone":"UTC"
     },
     "updated_at":"2018-04-10 22:22:09",
     "created_at":"2018-04-10 22:22:09",
     "id":4
  }
```                                      
### Updating a transaction: PUT api/transaction/{transactionId}
  - Request: amount
  - Response:transactionId, customerId, amount, date
```bash 
curl -X PUT http://isoftbet.test/api/transaction/1 \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer jBfQaAPJovnDJ62H6piLXe7zmFzkjlyMdrFsyxztbDNMZ9t8RgMwGY7BWOFx" \
  -d '{"amount":"2000.30"}'

```  
```json                                    
{  
   "id":1,
   "customer_id":1,
   "amount":"2000.30",
   "date":"2018-04-10 18:13:52",
   "created_at":"2018-04-10 18:13:52",
   "updated_at":"2018-04-10 22:24:06"
}
```                                        
  
### Deleting a transaction: DELETE api/transaction/{transactionId}
  - Request: ​
  - Response: success/fail 
```bash 
curl -X DELETE http://isoftbet.test/api/transaction/1 \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer jBfQaAPJovnDJ62H6piLXe7zmFzkjlyMdrFsyxztbDNMZ9t8RgMwGY7BWOFx" \

```                                        

## [Transactions](http://isoftbet.test/transaction)


## Crontab

```bash
PATH=/usr/bin/:/usr/sbin/:/bin/
MAILTO=dev@isoftbet.test
ARTISAN=/home/vagrant/www/isoftbet/artisan


# m   h   dom mon dow   command

57   23   */2   *   *     $ARTISAN stat -v --env=prod > /dev/null

```

