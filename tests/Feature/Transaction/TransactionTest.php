<?php

namespace Tests\Feature\Transaction;

use App\Customer;
use App\Transaction;
use App\User;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    public function testsTransactionIsCreatedCorrectly()
    {
        $data = [
            'customer_id' => 1,
            'amount' => 100,
        ];

        $this->json('POST', '/api/transaction', $data, $this->headers())
            ->assertStatus(201)
            ->assertJson(['id' => 1, 'customer_id' => 1, 'amount' => 100,]);
    }

    public function testsTransactionIsUpdatedCorrectly()
    {
        $transaction = factory(Transaction::class)->create([
            'amount' => 100,
        ]);

        $data = [
            'amount' => 200,
        ];

        $response = $this->json('PUT', '/api/transaction/' . $transaction->id, $data, $this->headers())
            ->assertStatus(200)
            ->assertJson([
                'id' => $transaction->id,
                'customer_id' => $transaction->customer_id,
                'amount' => 200,
            ]);
    }

    public function testsTransactionIsGottenCorrectly()
    {
        $transaction = factory(Transaction::class)->create([
            'customer_id' => 1,
            'amount' => 100,
        ]);

        $response = $this->json('GET', '/api/transaction/' . $transaction->id, [], $this->headers())
            ->assertStatus(200)
            ->assertJson([
                'id' => $transaction->id,
                'customer_id' => 1,
                'amount' => 100,
            ]);
    }

   public function testsTransactionIsDeletedCorrectly()
    {
        $transaction = factory(Transaction::class)->create();

        $response = $this->json('DELETE', '/api/transaction/' . $transaction->id, [], $this->headers())
            ->assertStatus(204);
    }

    /**
     * @return mixed
     */
    private function headers()
    {
        $user = factory(User::class)->create();
        $customer = factory(Customer::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        return $headers;
    }


}
