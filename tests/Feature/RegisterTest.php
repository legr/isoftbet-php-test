<?php

namespace Tests\Feature;

use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function testsRegistersSuccessfully()
    {
        $data = [
            'name' => 'Ivan',
            'email' => 'ivan@domain.com',
            'password' => '123456',
            'password_confirmation' => '123456',
        ];

        $this->json('post', '/api/register', $data)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);;
    }

    public function testsRequiresPasswordEmailAndName()
    {
        $this->json('post', '/api/register')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'name' => ['The name field is required.'],
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.'],
                ]]);
    }

    public function testsRequirePasswordConfirmation()
    {
        $data = [
            'name' => 'Ivan',
            'email' => 'ivan@domain.com',
            'password' => '123456',
        ];

        $this->json('post', '/api/register', $data)
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'password' => ['The password confirmation does not match.'],
                ]]);
    }
}
