<?php

namespace Tests\Feature\Customer;

use App\User;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    public function testsCustomerAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $data = [
            'name' => 'foo',
            'cnp' => 'base',
        ];

        $this->json('POST', '/api/customer', $data, $headers)
            ->assertStatus(201)
            ->assertJson(['id' => 1, 'name' => 'foo', 'cnp' => 'base']);
    }

}
