<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 10.04.18
 * Time: 23:30
 */

namespace Tests\Unit\Service;

use App\Service\StatService;
use App\Transaction;
use Carbon\Carbon;
use Tests\TestCase;

class StatServiceTest extends TestCase
{

    public function testExecute()
    {
        $t1 = factory(Transaction::class)->create(['amount' => 100, 'date' => Carbon::now()->subDays(3)]);
        $t2 = factory(Transaction::class)->create(['amount' => 500, 'date' => Carbon::yesterday()]);
        $t3 = factory(Transaction::class)->create(['amount' => 200, 'date' => Carbon::today()]);

        $from = Carbon::yesterday();
        $to = Carbon::today();
        $statService = new StatService();
        $stat = $statService->execute($from, $to);
        $this->assertEquals(500, $stat->sum);
    }
}
