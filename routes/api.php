<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {
});


Route::get('customer', 'CustomerController@index');
Route::get('customer/{customer}', 'CustomerController@show');
Route::post('customer', 'CustomerController@store');
Route::put('customer/{customer}', 'CustomerController@update');
Route::delete('customer/{customer}', 'CustomerController@delete');
Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::get('transaction', 'TransactionController@index');
Route::get('transaction/{transaction}', 'TransactionController@show');
Route::post('transaction', 'TransactionController@store');
Route::put('transaction/{transaction}', 'TransactionController@update');
Route::delete('transaction/{transaction}', 'TransactionController@delete');
